"""
    Converts VATROUTE routes to FMS plans for X-Plane which should be put in
    "X-Plane/Output/FMS Plans". Everything is encoded as Lon/Lat pair to ensure
    that all nav points are in the computer even if they are missing from the
    X-Plane nav data database (plus the points which exist in the data are often
    in a different place anyway).
    
    pip install requests
    pip install beautifulsoup4

    Example:
    python convert.py "http://www.vatroute.net/web_showfp.php?dep=EPKK&dest=LHBP&routing=67158" > EPKK_LHBP.fms
"""


import requests
import sys
from bs4 import BeautifulSoup


def parse(data):
    soup = BeautifulSoup(data, 'html.parser')
    routes = soup.find_all(class_="routes")
    rows = routes[-1].find_all('tr')[1:-1]
    rw = []
    for r in rows:
        tds = r.find_all('td')
        rw.append((tds[1].string, tds[2].string))
    return rw


def convert(waypoints):
    text = ""
    text += "I\r\n"
    text += "3 version\r\n"
    text += "1\r\n"
    text += "%s\r\n" % (len(waypoints) - 1)
 
    for w in waypoints:
        a = "+%s" % w[0] if not w[0].startswith('+') and not w[0].startswith('+') else w[0]
        b = "+%s" % w[1] if not w[1].startswith('+') and not w[1].startswith('+') else w[1]
        text += "28 %s_%s 0.000000 %s %s\r\n" % (a, b, w[0], w[1])

    return text


# download
url = sys.argv[1]
r = requests.get(url)
r.raise_for_status()

# parse and save
p = parse(r.text)
p = convert(p)
print(p)